# Om-Nom Bites #

### Тестовое задание для GLOBUS. ###

Приложение принимает \*.**TXT**-файл, выполняет с ним операции, описанные в задании, и отправляет результат в виде таблицы или \*.**CSV**-файла (по выбору пользователя).

Выполнены все усложнения, которые были перечислены в [задании](https://docs.google.com/document/d/1OZUF4BHdYl2vtM7XxDH_gsNjBcjWUvrJiZVMEyJEpRQ/edit).

#### Попробовать онлайн

Попробовать приложение в деле Вы можете [здесь.](http://mark42.ru:3000)

#### Используемые технологии
* Клиентская сторона: HTML5/CSS/JS.
* Серверная сторона: NodeJS.
* Дополнительные модули: WebBox (собственная разработка, подробнее [тут](https://github.com/hypersasha/npm-webbox))

#### Запуск на локальной машине
Для корректной работы приложения на локальной машине, необходимо клонировать этот репозиторий в любую папку и установить в неё некоторые зависимости.

```
npm install webbox
npm install express
npm install multiparty
```

#### Превью
![preview.gif](https://psv4.vk.me/c812421/u28455889/docs/f78ea7ac541c/upload.gif?extra=hFTc5Gtv2qJ6LJMWVyej89ira99zG_zk-VxQqIPMkYGKB4ztN7acIbRSgwhiYJGtZMJanKlSZ2fC7BQApI-Wht2oB3FsgEuSQBHO0QVVu6H004mNGMZBJw)

![good.png](https://bitbucket.org/repo/RdGKzr/images/2337521494-good.png)