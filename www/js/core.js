var OmNom, whead, welt, OmNomTable;
var errHide = false;
var filename = null;
var ready = null;

var fp = new filePicker(false);
fp.onchange(prepareFile);

function start() {
    OmNom = document.getElementById('omnom');
    whead = document.getElementById('welcome-title');
    welt = document.getElementById('welcome-text');
    OmNomTable = document.getElementById('table');

    // Get last uploads
    getLastUploads();

    if (typeof(window.FileReader) == 'undefined') {
        loadDefault();
        welt.innerHTML = 'Нажмите на <b>Ням-Няма</b>, чтобы выбрать файл для загрузки.';
    } else {
        loadOmNom();
    }
}

function cancel(e) {
    e = e || window.event;
    if (e.preventDefault) { e.preventDefault(); }
    return false;
}

function getCSV() {
    if (!filename) {
        composeError('Сначала нужно загрузить файл!');
        return;
    } else {
        showLoader();
        var params = {
            url: '/getCsv',
            method: 'POST',
            data: {filename: filename},
            success: onGotCSV
        };
        XmlRequest(params, onGotCSV);
    }
}

function getTable() {
    if (!filename) {
        composeError('Сначала нужно загрузить файл!');
        return;
    } else {
        showLoader();
        var params = {
            url: '/getTable',
            method: 'POST',
            data: {filename: filename},
            success: onGotTable
        };
        XmlRequest(params);
    }
}

function getLastUploads() {
    var params = {
        url: '/getLast',
        method: 'POST',
        success: onLastLoaded
    };
    XmlRequest(params);
}

function loadDefault() {
    OmNom.onclick = function () {
        fp.pick();
    }

    addEventHandler(OmNom, 'mouseenter', function (e) {
        if (ready) clearInterval(ready);
        addClass(OmNom, 'ready');
        cancel(e);
    });

    addEventHandler(OmNom, 'mouseleave', function (e) {
        ready = setTimeout(function () {
            removeClass(OmNom, 'ready');
        }, 150);
        cancel(e);
    });
}

function loadOmNom() {

    // Load default first.
    loadDefault();

    addEventHandler(OmNom, 'drop', onDropped);

    addEventHandler(OmNom, 'dragover', function (e) {
        addClass(OmNom, 'ready');
        cancel(e);
    });

    addEventHandler(OmNom, 'dragenter', function (e) {
        addClass(OmNom, 'ready');
        cancel(e);
    });

    addEventHandler(OmNom, 'dragleave', function (e) {
        removeClass(OmNom, 'ready');
        cancel(e);
    });
}

function makeTableRow(cols) {
    var row = document.createElement('div');
    row.className = 'table-row';
    
    // Generate cols and paste it to row
    var col = null;
    cols.forEach(function (val) {
        col = document.createElement('div');
        col.className = 'table-col';
        col.innerHTML = val;
        row.appendChild(col);
    });

    return row;
}

function onDropped(e) {
    e = e || window.event;
    if (e.preventDefault) { e.preventDefault(); }

    removeClass(OmNom, 'ready');

    // Get dropped file
    var dt = e.dataTransfer;
    var file = dt.files[0];

    if (file) {
        var files = [file];
        uploadFile(files);
    } else {
        composeError("Вы должны выбрать файл!");
    }
}

function onError() {
    hideLoader();
    composeError('Не удалось загрузить файл!');
    removeClass(OmNom, 'loading');
}

function onGotCSV(data) {
    hideLoader();
    if (data.response.status === 1) {
        window.location = window.location + "download?name=" + data.response.filename;
        document.getElementById('link').style.display = "block";
        document.getElementById('link').innerHTML = window.location + "download?name=" + data.response.filename;
        getLastUploads();
    } else if (data.response.status === 513) {
        composeError('Неверный файл! В файле допустимы только цифры!');
        restart();
    } else {
        composeError('Не удалось создать файл!');
        restart();
    }
}

function onGotTable(data) {
    hideLoader();
    var status = data.response.status;
    if (status === 513) {
        composeError('Файл может содержать только цифры!');
        restart();
        return;
    }

    var result = data.response.result;

    // Clear Table
    OmNomTable.innerHTML = "";

    // Generate table head
    var head = document.createElement('div');
    head.id = 'table-head';
    head.className = 'table-row';
    head.innerHTML = '<div class="table-col">Число</div>' +
        '<div class="table-col">Множитель</div>' +
        '<div class="table-col">Результат</div>';
    OmNomTable.appendChild(head);

    var colValues, row;
    result.forEach(function (num) {
        colValues = [num/2, 2, num];
        row = makeTableRow(colValues);
        OmNomTable.appendChild(row);
    });
}

function onLastLoaded(data) {
    var last = data.response.last;
    var lastBox = document.getElementById('last');
    lastBox.innerHTML = '<h3>Недавно обработанные:</h3>';
    if (last.length === 0) {
        lastBox.innerHTML = "";
    } else {
        for (var i = last.length-1; i > last.length - 5; i--) {
            if (!last[i]) break;
            lastBox.innerHTML += '<a href="'+window.location+'download?name='+last[i]+'" target="_blank">'+last[i]+'</a>';
        }
    }
}

function onSuccess(data) {
    hideLoader();
    var myfile = data.response.files[0];
    if (!myfile.saved) {
        switch (myfile.errors.code) {
            case 413:
                composeError('Максимальный размер файла - 2 MB!');
                break;
            case 415:
                composeError('Допустимы только *.txt файлы!');
                break;
            default:
                composeError('Не удалось сохранить файл!');
        }
        removeClass(OmNom, 'loading');
        return;
    }

    whead.innerHTML = "Ням-Ням!";
    welt.innerHTML = "Ваш файл успешно загружен! Что бы Вы хотели с ним сделать?";
    removeClass(OmNom, 'loading');
    document.getElementById('gui').style.display = "block";
    OmNom.style.display = "none";
    document.getElementById('refresh').style.display = "block";
    filename = myfile.name;
}

function prepareFile(f) {
    if (!fp.isEmpty()) {
        uploadFile(f);
    } else {
        composeError('Вы должны выбрать файл.');
    }
}

function restart() {
    filename = null;
    OmNom.style.display = "block";
    OmNomTable.innerHTML = "";
    document.getElementById('gui').style.display = "none";
    document.getElementById('refresh').style.display = "none";
    document.getElementById('link').style.display = "none";
    whead.innerHTML = "Ещё раз?";
    welt.innerHTML = 'Почему бы и нет! Попробуйте загрузить другой файл.';
}

function showLoader() {
    document.getElementsByClassName('loader-icon')[0].style.display = "block";
}

function hideLoader() {
    document.getElementsByClassName('loader-icon')[0].style.display = "none";
}

function uploadFile(files) {

    if (files[0].type != "text/plain") {
        composeError('Могут быть загружены только *.TXT-файлы!');
        //return;
    }

    // Check file size
    if (files[0].size / 1000000 > 2) {
        composeError('Максимальный размер файла - 2 MB.');
        return;
    }
    showLoader();
    addClass(OmNom, 'loading');
    var params = {
        url: '/newfile',
        method: 'POST',
        data: {msg: 'OmNom.'},
        file: files,
        error: onError
    };
    XmlRequest(params, function (data) {
        setTimeout(function() {
            onSuccess(data);
        }, 500);
    });
}

// Sub-Functions

function composeError(text) {
    clearInterval(errHide);
    var errbox = document.getElementById('error');
    document.getElementById('error-text').innerHTML = text;
    addClass(errbox, 'err-vis');
    startHideCounter();
}

function startHideCounter() {
    errHide = setInterval(hideError, 3000);
}

function hideError() {
    var errbox = document.getElementById('error');
    removeClass(errbox, 'err-vis');
    clearInterval(errHide);
}

function addEventHandler(obj, evt, handler) {
    if(obj.addEventListener) {
        obj.addEventListener(evt, handler, false);
    } else if(obj.attachEvent) {
        obj.attachEvent('on'+evt, handler);
    } else {
        obj['on'+evt] = handler;
    }
}

function addClass(o, c){
    var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g")
    if (re.test(o.className)) return;
    o.className = (o.className + " " + c).replace(/\s+/g, " ").replace(/(^ | $)/g, "")
}

function removeClass(o, c){
    var re = new RegExp("(^|\\s)" + c + "(\\s|$)", "g")
    o.className = o.className.replace(re, "$1").replace(/\s+/g, " ").replace(/(^ | $)/g, "")
}