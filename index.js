const web = require('webbox');
const fs = require('fs');
const rl = require('readline');
const path = require('path');

var lastUploads = [];

var server = new web.Server.Box();
server.createServer(3000);
server.setServerRoot('www/');


server.onPost('/getCsv', composeCSV);

server.onPost('/getTable', composeTable);

server.onGet('/download', downloadCSV);

server.onPost('/getLast', showLast);

var vd = new web.VirtualDrive.Box(server, 'VD/');
var properties = {
    maxSize: 2*1024*1024,
    fileTypes: ['text/plain'],
    maxNameLength: 4
};

vd.onUpload('/newfile', '/omnom', properties, function(req, res, result){
    server.sendJSON(res, result);
});

function composeCSV (req, res) {
    var filename = req.body.filename;
    var failed = false;
    var result = "";

    // Check file for existing
    var check = web.Server.Box.fileExists('./www/VD/omnom/', filename);
    if (!check.fileExists) {
        server.sendJSON(res, {status: 513});
        return;
    }

    var lineReader = rl.createInterface({
        input: fs.createReadStream('./www/VD/omnom/'+filename)
    });

    lineReader.on('line', function (line) {
        var math = parseInt(line);
        if (isNaN(math)) {
            failed = true;
        }
        math = math * 2;
        result += line + ",2," + math.toString() + "\r\n";
    }).on('close', function () {
        filename = filename.substring(0, 4);
        fs.writeFile('./www/VD/done/converted_'+filename+'.csv', result, function(err) {
            if (err) throw err;
            if (failed) {
                server.sendJSON(res, {status: 513, filename: filename});
                fs.unlink('./www/VD/done/converted_'+filename+'.csv');
                fs.unlink('./www/VD/omnom/'+filename+'.txt');
            } else {
                server.sendJSON(res, {status: 1, filename: filename});

                if (lastUploads.indexOf(filename) < 0)
                    lastUploads.push(filename);

                if (lastUploads.length > 4) {
                    lastUploads.splice(0, lastUploads.length-5);
                }
            }
        });
    });
}

function composeTable(req, res) {
    var filename = req.body.filename;
    var failed = false;
    var result = [];

    // Check file for existing
    var check = web.Server.Box.fileExists('./www/VD/omnom/', filename);
    if (!check.fileExists) {
        server.sendJSON(res, {status: 513});
        return;
    }

    var lineReader = rl.createInterface({
        input: fs.createReadStream('./www/VD/omnom/'+filename)
    });

    lineReader.on('line', function (line) {
        var math = parseInt(line);
        if (isNaN(math)) {
            failed = true;
        }
        math = math * 2;
        result.push(math);
    }).on('close', function () {
        if (failed) {
            server.sendJSON(res, {status: 513});
            fs.unlink('./www/VD/omnom/'+filename);
        } else {
            server.sendJSON(res, {status: 1, result: result});
        }
    });
}

function downloadCSV (req, res) {
    var name = req.query.name;
    var file = './www/VD/done/converted_' + name + '.csv';
    var check = web.Server.Box.fileExists('./www/VD/done/', 'converted_' + name + '.csv');
    if (check.fileExists) {
        res.download(file);
    } else {
        res.sendStatus(404);
    }
}

function showLast(req, res) {
    server.sendJSON(res, {last: lastUploads});
}